require('dotenv').config();

const axios = require("axios");

const BASE_BACKEND_URL = process.env.BACKEND_URL;

async function getTopics() {
    
    try {
        const topics = await axios.get(`${BASE_BACKEND_URL}topics`).then(function (response) {
            const { data: responseBody } = response;
            const { data } = responseBody;
            const { data: topics } = data;
            return topics;
        });
        return topics;   
    } catch (e) {
        return e.message
    }
}


async function getSubTopics(topicId) {

    try {
        const subTopics = await axios.get(`${BASE_BACKEND_URL}topics/${topicId}/sub-topics`).then(function (response) {
            const { data: responseBody } = response;
            const { data } = responseBody;
            const { data: subTopics } = data;
            return subTopics;
        });
        return subTopics;
    } catch (e) {
        return e.message
    }
}


async function getQuestions(subTopicId) {
    try {
        const questions = await axios.get(`${BASE_BACKEND_URL}sub-topics/${subTopicId}`).then(function (response) {
            const { data: responseBody } = response;
            const { data } = responseBody;
            const { data: questions } = data;
            return questions;
        });
        return questions;
    } catch (e) {
        return e.message
    }
}


module.exports = {
    getTopics,
    getSubTopics,
    getQuestions
}
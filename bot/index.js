require('dotenv').config();

const { Telegraf } = require('telegraf');
const Stage = require('telegraf/stage')
const session = require('telegraf/session');

const menuScene = require("./scenes/menu");
const quizScene = require("./scenes/quiz");

const {leave} = Stage;
const bot = new Telegraf(process.env.BOT_TOKEN)
const stage = new Stage([menuScene, quizScene]);

stage.command('cancel', leave())

bot.use(session())
bot.use(stage.middleware())

bot.command(["start", "menu"], (ctx) => ctx.scene.enter('menu'))

bot.command("quiz", (ctx) => ctx.scene.enter('quiz'))

bot.startPolling().catch((error, ctx) => {
    ctx.reply("Bot-un işə düşməsi zamanı xəta baş veri xaiş edirik bir az sonra bir daha yoxlayın");
})


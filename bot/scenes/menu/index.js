const Scene = require('telegraf/scenes/base');

const enterHandler = require("./enter");
const startActionHandler = require("./actions/start");

const menuScene = new Scene('menu');

menuScene.enter(enterHandler);
menuScene.action("start", startActionHandler);

module.exports = menuScene
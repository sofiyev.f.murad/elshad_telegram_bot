const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')

module.exports = async function enterHandler(ctx) {

    ctx.session.quiz = {
        topics: {
            pagination: {},
            data: null
        },
        subTopics: {
            pagination: {},
            data: null
        },
        questions: {
            pagination: {},
            data: null
        },
        currentQuestion: {
            index: null,
            answer: null
        }
    }

    ctx.session.user = {
        score: 0
    }

    ctx.reply(
        `Ilk öncə kateqroiya seçməlisiniz`,
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard([
                m.callbackButton(`Bütün kateqoriyalara bax`, "start"),
            ]),
        )
    );
}


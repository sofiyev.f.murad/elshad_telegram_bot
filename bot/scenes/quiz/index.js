const Scene = require('telegraf/scenes/base');

const enterHandler = require("./enter");
const messageHandler = require("./message");

const quizScene = new Scene('quiz');

quizScene.enter(enterHandler);
quizScene.on("message", messageHandler);
quizScene.action("start", messageHandler);
quizScene.on('callback_query', messageHandler)

module.exports = quizScene
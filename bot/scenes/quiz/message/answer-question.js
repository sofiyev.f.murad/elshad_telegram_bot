const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');
const {stringNormalize, shuffleWord} = require("../../../utils");
module.exports = async function handleAnswerQuestion(ctx) {
    const {
        session: {
            quiz: {
                subTopics,
                questions,
                currentQuestion
            },
            user
        },
        message
    } = ctx;

    const messageTextNormalized = stringNormalize(message.text);
    if(stringNormalize(currentQuestion.answer) === messageTextNormalized) {
        user.score = ++user.score
        if(questions.data.length - 1 <= currentQuestion.index) {
            questions.data = null
            ctx.reply(`Suallar qutardı sizin ümumi xalınız ${user.score}`);

            ctx.reply(
                `Aşağıdakı səviyyələrdən birini seçin`,
                Extra.HTML().markup((m) =>
                    Markup.inlineKeyboard(subTopics.data.slice().map((subTopic) => {
                        return  m.callbackButton(`${subTopic.sub_name}`, `/${stringNormalize(subTopic.sub_name)}`);
                    }))
                )
            );

        }else {
            currentQuestion.index = ++currentQuestion.index;
            currentQuestion.question = questions.data[currentQuestion.index].question;
            currentQuestion.answer = questions.data[currentQuestion.index].answer;

            ctx.replyWithHTML(`Sual - <b> ${currentQuestion.question}</b>\nCavab - <b> ${shuffleWord(currentQuestion.answer)} </b>`)
        }
    }else {
        ctx.reply(`Yanlış cavab!`)
    }

}
const { getCurrentStepFromSession } = require("../../../utils");
const topics = require("./topics");
const subTopics = require("./sub-topic");
const question = require("./question");
const answerQuestion = require("./answer-question");

module.exports = async function messageHandler(ctx) {
    const currentStep = getCurrentStepFromSession(ctx.session);
    switch (currentStep) {
        case "TOPIC": {
            topics(ctx);
            return
        }
        case "SUB_TOPIC": {
            subTopics(ctx);
            return;
        }
        case "QUESTION": {
            question(ctx);
            return;
        }
        case "ANSWER_QUESTION": {
            answerQuestion(ctx);
            return;
        }
        default: {

        }
    }
}
const {getQuestions} = require("../../../../QUIZ_SDK");
const {shuffleWord, stringNormalize, getSubTopicFromMessage} = require("../../../utils");

module.exports = async function handleQuestionMessage(ctx) {
    const {
        session: {
            quiz: {
                subTopics,
                questions,
                currentQuestion
            }
        },
        message,
        callbackQuery
    } = ctx;

    const messageTextNormalized = stringNormalize(message ? message.text : callbackQuery.data);

    const subTopic = getSubTopicFromMessage(subTopics.data, messageTextNormalized);

    if (subTopic) {
        questions.data = await getQuestions(subTopic.id);
        currentQuestion.index = 0;
        currentQuestion.question = questions.data[0].question;
        currentQuestion.answer = questions.data[0].answer;

        ctx.replyWithHTML(`Sual - <b> ${currentQuestion.question}</b>\nCavab - <b> ${shuffleWord(currentQuestion.answer)} </b>`)
    }
}
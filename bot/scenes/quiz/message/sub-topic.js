const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');
const { getSubTopics } = require("../../../../QUIZ_SDK");
const { stringNormalize, getTopicFromMessage } = require("../../../utils");
module.exports = async function handleSubTopicMessage(ctx) {

    const {
        session: {
            quiz: {
                topics: {
                    data: topicsData
                },
                subTopics
            }
        },
        message
    } = ctx;

    const messageTextNormalized = stringNormalize(message.text);

    const topic = getTopicFromMessage(topicsData, messageTextNormalized);

    if(topic) {
        subTopics.data = await getSubTopics(topic.id);

        ctx.reply(
            `Aşağıdakı səviyyələrdən birini seçin`,
            Extra.HTML().markup((m) =>
                Markup.inlineKeyboard(subTopics.data.slice().map((subTopic) => {
                    return  m.callbackButton(`${subTopic.sub_name}`, `/${stringNormalize(subTopic.sub_name)}`);
                }))
            )
        );

        // let subTopicsResponse = ``;
        //
        // subTopics.data.forEach((subTopic) => {
        //     subTopicsResponse += `/${stringNormalize(subTopic.sub_name)} \n`;
        // })
        //
        // ctx.reply(subTopicsResponse);
    }
}
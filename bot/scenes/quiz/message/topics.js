const {getTopics} = require("../../../../QUIZ_SDK");
const {chunkArray} = require("../../../utils");

module.exports = async function handleTopicsMessage(ctx) {
    const {
        session: {
            quiz: {
                topics
            }
        }
    } = ctx;

    const topicsData = await getTopics();
    topics.data = topicsData;

    const topicsMediaGroup = topicsData.map((topic) => {
        let {
            image_name: topicPhoto,
            topic_name: topicName,
        } = topic;

        topicName = `/${topicName.toLowerCase()}`;

        return {
            media: topicPhoto,
            caption: topicName,
            type: 'photo'
        }
    });

    const chunkedTopicsMediaGroup = chunkArray(topicsMediaGroup, 5);

    for (const chunkArrayItem of chunkedTopicsMediaGroup) {
        await ctx.replyWithMediaGroup(chunkArrayItem)
    }
}
function shuffleWord(string) {
    let shuffle = [...string];
    const getRandomValue = (i, N) => Math.floor(Math.random() * (N - i) + i);
    shuffle.forEach((elem, i, arr, j = getRandomValue(i, arr.length)) => [arr[i], arr[j]] = [arr[j], arr[i]]);
    shuffle = shuffle.join('');
    return shuffle;
}

function chunkArray(myArray, chunk_size) {
    var results = [];

    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }

    return results;
}

function stringNormalize(str) {
    return str.toLowerCase().replace(/\s/g, '').replace(/^\/|\/$/g, '');
}


function getCurrentStepFromSession(session) {
    let {
        quiz: {
            topics: {
                data: topicsData
            },
            subTopics: {
                data: subTopicsData
            },
            questions: {
                data: questionsData
            },
            currentQuestion: {
                answer
            },
        },
    } = session;

    if(!topicsData) {
        return "TOPIC"
    }

    if(!subTopicsData) {
        return "SUB_TOPIC"
    }

    if(!questionsData) {
        return "QUESTION"
    }

    if(answer) {
        return "ANSWER_QUESTION"
    }
}


function getTopicFromMessage(topics, message) {
    return topics.find((topic) => {
        let { topic_name } = topic;
        topic_name = stringNormalize(topic_name);
        return topic_name === message;
    });
}

function getSubTopicFromMessage(subTopics, message) {
    return subTopics.find((topic) => {
        let { sub_name } = topic;
        sub_name = stringNormalize(sub_name);
        return sub_name === message;
    });
}



module.exports = {
    shuffleWord,
    chunkArray,
    stringNormalize,
    getCurrentStepFromSession,
    getTopicFromMessage,
    getSubTopicFromMessage
}